# My Fvwm Configuration for everyday work

## History
I like to configure my desktop as **I** want not just "as other think is standard". Thus FvWM is my WM of choice for more than 10 years now.
This configuration is based on the Ubuntu 18.10 default config where I added some of my favorite FvwmButtons and bindings.


## Includes:
- Quake Terminal running screen in RXvt Unicode
- Run app command using gmrun
- Conky
- Bindings

## Screenshot
How it looks on my triple screen @work:
![Default setup!](screenshot.png)

##Requirements (not mandatory, adjust the config in case)
- Terminal: RXvt Unicode
- Imagemagick
- Conky
- hsetroot
- copyq
- gmrun
- screen
- vim
- bash
- toilet

## Other
You might be interested in the second repository to grab some alias/configurations: [Dotfile repository](https://github.com/warnaud/dotfiles)
Source of inspiration and placeholder for tons of other configurations:[Tons of Fvwm Configs](http://arnaud.fortier-family.com/fvwm/)



Have fun !
